#include <Wire.h>

//See http://datasheets.maximintegrated.com/en/ds/DS1621.pdf

//Commands
#define READ_TEMPERATURE 0xAA
#define ACCESS_TH 0xA1
#define ACCESS_TL 0xA2
#define ACCESS_CONFIG 0xAC
#define READ_COUNTER 0xA8
#define READ_SLOPE 0xA9
#define START_CONVERT 0xEE
#define STOP_CONVERT 0x22

//DS1621 4-bit control code is 1001
//The next 3 bits are the device select bits A2 A1 A0
//The Wire library handles setting the read/write bit
//which means we only need the 7 bits
#define SENSOR1 B1001100
#define SENSOR2 B1001000

/*
 Config bit for DS1621
 DONE | THF | TLF | NVB | X | X | POL | 1SHOT 
*/

byte oneShotConfig = B00000001;
byte doneMask = B10000000;

void setup() {
    Serial.begin(9600);
    
    Wire.begin();

    writeConfig(SENSOR1, oneShotConfig);
    writeConfig(SENSOR2, oneShotConfig);

    Serial.println(F("One Degree - Half Degree - High Res - Temp in Fahrenheit"));
}

void loop() {
    Serial.println(F("------------------------------"));
    printTempToSerial(SENSOR1);
    printTempToSerial(SENSOR2);

    delay(10000);
}

void printTempToSerial(int deviceId) {
    oneShotConversion(deviceId);
    
    char oneDegreeResolution = getOneByteTemp(deviceId);
    //Passing DEC so that print will print the number not the ASCII character
    Serial.print(oneDegreeResolution, DEC);

    float halfDegreeResolution = getTwoByteTemp(deviceId);
    Serial.print("C - ");
    Serial.print(halfDegreeResolution);

    float highRes = getHighResolutionTemp(deviceId);
    Serial.print("C - ");
    Serial.print(highRes);

    float tempF = celsiusToFahrenheit(highRes);
    Serial.print("C - ");
    Serial.print(tempF);
    Serial.println("F");
}

float celsiusToFahrenheit(float celsius) {
    return celsius * (9.0f / 5.0f) + 32.0f;
}

bool isConversionDone(int deviceId) {
    Wire.beginTransmission(deviceId);
    Wire.write(ACCESS_CONFIG);
    Wire.endTransmission();
    Wire.requestFrom(deviceId, 1);

    byte config = Wire.read();
    return config & doneMask;
}

void oneShotConversion(int deviceId) {
    requestConversion(deviceId);
    waitForConversionToFinish(deviceId);
}

void waitForConversionToFinish(int deviceId) {
    //This method just blocks until the conversion
    //is complete
    while (!isConversionDone(deviceId)) {
    } 
}

void readTemperature(int deviceId) {
    Wire.beginTransmission(deviceId);
    Wire.write(READ_TEMPERATURE);
    Wire.endTransmission();
}

char getOneByteTemp(int deviceId) {
    readTemperature(deviceId);

    Wire.requestFrom(deviceId, 1);
    
    //The value returned from the DS1621
    //is a signed byte (Two's complement)
    //casting as a char because a char is a signed byte
    return (char)Wire.read();
}

float getTwoByteTemp(int deviceId) {
    readTemperature(deviceId);
    Wire.requestFrom(deviceId, 2);
    float temp = (char)Wire.read();

    byte halfDegree = Wire.read();
    if (halfDegree) {
        temp += 0.5;
    }
    
    return temp;
}

float getHighResolutionTemp(int deviceId) {
    char baseTemp = getOneByteTemp(deviceId);

    Wire.beginTransmission(deviceId);
    Wire.write(READ_COUNTER);
    Wire.endTransmission();
    
    Wire.requestFrom(deviceId, 1);
    float countRemain = Wire.read();

    Wire.beginTransmission(deviceId);
    Wire.write(READ_SLOPE);
    Wire.endTransmission();
    
    Wire.requestFrom(deviceId, 1);
    float countPerC = Wire.read();

    /*
    Serial.print("read counter ");
    Serial.println(countRemain);

    Serial.print("countPerC ");
    Serial.println(countPerC);
    */

    return (float(baseTemp) - 0.25) + ((countPerC - countRemain) / countPerC);
}


void writeConfig(int deviceId, byte config) {
    Wire.beginTransmission(deviceId);
    Wire.write(ACCESS_CONFIG);
    Wire.write(config);
    Wire.endTransmission();
}

void requestConversion(int deviceId) {
    Wire.beginTransmission(deviceId);
    Wire.write(START_CONVERT);
    Wire.endTransmission();
}
